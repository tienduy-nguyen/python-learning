# A List is a collection which is ordered and changeable. Allows duplicate members.

# Create list
# numbers = [1,2,3,4,5]
# Using a constructor
numbers = list((1, 2, 3, 4, 5))
numbers2 = range(1, 10)
fruits = ["Apples", "Oranges", "Grapes", "Pears"]
print(numbers2)

# Get value
print(fruits[1])

# Get len
print(len(fruits))

# Append to list
fruits.append("Mangos")

# Remove from list
fruits.remove("Grapes")
array = [10, 20, 30, 40, 40, 40, 41, 42, 50, 60, 70, 80, 90, 100]
array.remove(40)  # remove the first item  = 40
index40 = array.index(40)
print(array)
array.pop(index40)
print(array)
index40 = array.index(40)
del array[index40]
print(array)

array = [10, 20, 30, 40, 40, 40, 41, 42, 50, 60, 70, 80, 90, 100]
num = 40, 41, 42
newList = [i for i in array if i not in num]
list2 = list(filter(lambda x: x not in num, array))
print(newList, list2)

# Insert into position
fruits.insert(2, "Strawberries")

# Remove from position
fruits.pop(3)

# Reverse list
fruits.reverse()

# Sort list
fruits.sort()

# Reverse sort
sorted(fruits)
fruits.sort(reverse=True)

print(fruits)
